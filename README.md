# Smarter Scroll

The motivation behind this project is to convert people to use Natural Scroll direction on MacBook trackpads. The hope was that even though these people prefer to scroll down by moving their fingers down, left and right should still be natural. Encasing a horizontal scroll inside of a vertical one would should them the error of their ways :)
