import './index.scss';

let scrollWindow;
let scrollVertical;
let scrollHorizontal;
let scrollContent;
let contentLeft;
let contentMiddle;
let contentRight;
let labelTop;
let labelBottom;
let labelLeft;
let labelRight;

const wheel = ({ deltaX, deltaY }) => {
  console.log(deltaY);
};
const init = () => {
  scrollWindow = document.createElement('div');
  scrollVertical = document.createElement('div');
  scrollHorizontal = document.createElement('div');
  scrollContent = document.createElement('div');
  contentLeft = document.createElement('div');
  contentMiddle = document.createElement('div');
  contentRight = document.createElement('div');

  labelTop = document.createElement('p');
  labelBottom = document.createElement('p');
  labelLeft = document.createElement('p');
  labelRight = document.createElement('p');

  labelTop.innerText = 'Scroll Vertically';
  labelBottom.innerText = 'Now Scroll horizontally';
  labelLeft.innerText = 'Good Scroll!';
  labelRight.innerText = 'Bad Scroll!';
  scrollWindow.className = 'window';
  scrollVertical.className = 'window-vertical';
  labelTop.className = 'label-top';
  scrollHorizontal.className = 'window-horizontal';
  scrollContent.className = 'window-content';
  contentLeft.className = 'window-content-left';
  contentMiddle.className = 'window-content-middle';
  contentRight.className = 'window-content-right';

  contentMiddle.appendChild(labelBottom);
  contentLeft.appendChild(labelLeft);
  contentRight.appendChild(labelRight);

  scrollWindow.appendChild(scrollVertical);
  scrollVertical.appendChild(labelTop);
  scrollVertical.appendChild(scrollHorizontal);

  scrollHorizontal.appendChild(scrollContent);
  scrollContent.appendChild(contentLeft);
  scrollContent.appendChild(contentMiddle);
  scrollContent.appendChild(contentRight);
  document.body.appendChild(scrollWindow);

  scrollHorizontal.scrollLeft = scrollHorizontal.getBoundingClientRect().width;

  window.addEventListener('wheel', wheel);
};


init();
